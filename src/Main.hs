module Main where

-- core
import XMonad
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.SpawnOnce
import XMonad.Util.ClickableWorkspaces

-- not XMonad
import System.Exit (exitSuccess)

-- actions
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWindows
import XMonad.Actions.Minimize
import XMonad.Actions.Promote
import XMonad.Actions.Submap
import XMonad.Actions.WindowGo (runOrRaise)

-- hooks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeWindows
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP

-- layouts
import XMonad.Layout.Accordion
import XMonad.Layout.ResizableTile
import XMonad.Layout.Simplest
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

-- layout modifiers
import qualified XMonad.Layout.BoringWindows as BW
import XMonad.Layout.LayoutModifier
import qualified XMonad.Layout.Magnifier as MG
import XMonad.Layout.Minimize
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import qualified XMonad.Layout.Renamed as R
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation

-- prompt
import XMonad.Prompt
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.Man
import XMonad.Prompt.Shell
import XMonad.Prompt.Window

-- user values
myTerminal :: String
myTerminal = "alacritty"

myFont :: String
myFont = "xft:Iosevka:size=10"

myModMask :: KeyMask
myModMask = mod4Mask

myBrowser :: String
myBrowser = "qutebrowser"

-- colors
foreground :: String
foreground = "#575279"
background :: String
background = "#faf4ed"
color0 :: String
color0 = "#faf4ed"
color1 :: String
color1 = "#b4637a"
color2 :: String
color2 = "#56949f"
color3 :: String
color3 = "#ea9d34"
color4 :: String
color4 = "#286983"
color5 :: String
color5 = "#908aa9"
color6 :: String
color6 = "#d7827e"
color7 :: String
color7 = "#575279"
color8 :: String
color8 = "#9e9ab6"
color9 :: String
color9 = "#eb6f92"
color10 :: String
color10 = "#9ccfd8"
color11 :: String
color11 = "#f6c177"
color12 :: String
color12 = "#31748f"
color13 :: String
color13 = "#c4a7e7"
color14 :: String
color14 = "#ebbcba"
color15 :: String
color15 = "#e0def4"

-- keybindings
myKeys :: [(String, X ())]
myKeys =
    -- xmonad stuff
    [ ("M-C-r", spawn "xmonad --recompile")
    , ("M-S-r", spawn "xmonad --restart")
    , ("M-S-q", io exitSuccess)

    -- launchers (p for program)
    , ("M-<Return>", spawn myTerminal)
    , ("M-p w", runOrRaise "qutebrowser" (className =? "qutebrowser"))
    , ("M-p S-w", runOrRaise "chromium" (className =? "Chromium"))
    , ("M-p e", spawn (myTerminal ++ " -e nvim"))
    , ("M-p m", spawn (myTerminal ++ " -e neomutt"))
    , ("M-p f", spawn (myTerminal ++ " -e lf"))
    , ("M-p o", spawn (myTerminal ++ " -e htop"))
    , ("M-p p", spawn "zathura")
    , ("M-p v", spawn "~/bin/watchvideo")
    , ("M-p S-v", spawn "~/bin/dlvideo")

    -- prompts (d for dmenu)
    , ("M-d p", shellPrompt myXPConfig)
    , ("M-d m", manPrompt myXPConfig)
    , ("M-d g", windowPrompt myXPConfig Goto allWindows)
    , ("M-d b", windowPrompt myXPConfig Bring allWindows)

    -- manage windows
    , ("M-q", kill1)
    , ("M-<Tab>", BW.focusDown)
    , ("M-S-<Tab>", BW.focusUp)
    , ("M-S-<Return>", promote)
    , ("M-l", sendMessage $ Go R)
    , ("M-k", sendMessage $ Go U)
    , ("M-j", sendMessage $ Go D)
    , ("M-h", sendMessage $ Go L)
    , ("M-S-l", sendMessage $ Swap R)
    , ("M-S-k", sendMessage $ Swap U)
    , ("M-S-j", sendMessage $ Swap D)
    , ("M-S-h", sendMessage $ Swap L)
    , ("M-n", withFocused minimizeWindow)
    , ("M-C-n", withLastMinimized maximizeWindowAndFocus)
    , ("M-C-<Space>", withFocused $ windows . W.sink)

    -- sublayouts (g for group)
    , ("M-g h", sendMessage $ pullGroup L)
    , ("M-g j", sendMessage $ pullGroup D)
    , ("M-g k", sendMessage $ pullGroup U)
    , ("M-g l", sendMessage $ pullGroup R)
    , ("M-g m", withFocused (sendMessage . MergeAll))
    , ("M-g u", withFocused (sendMessage . UnMerge))
    , ("M-g S-u", withFocused (sendMessage . UnMergeAll))
    , ("M-g <Tab>", onGroup W.focusDown')
    , ("M-g S-<Tab>", onGroup W.focusUp')
    , ("M-g <Space>", toSubl NextLayout)

    -- manage layouts
    , ("M-<Space>", sendMessage NextLayout)
    , ("M-,", sendMessage Shrink)
    , ("M-.", sendMessage Expand)
    , ("M-S-,", sendMessage MirrorShrink)
    , ("M-S-.", sendMessage MirrorExpand)
    , ("M-=", incScreenWindowSpacing 5)
    , ("M--", decScreenWindowSpacing 5)
    , ("M-f", sendMessage $ Toggle FULL)
    , ("M-C-f", sendMessage $ Toggle MIRROR)
    , ("M-b", sendMessage ToggleStruts)
    , ("M-m", sendMessage MG.Toggle)

    -- user
    , ("<XF86AudioPlay>", spawn "playerctl play-pause")
    , ("<Pause>", spawn "playerctl play-pause")
    , ("<XF86AudioLowerVolume>", spawn "pactl set-sink-volume 0 -5%")
    , ("<XF86AudioRaiseVolume>", spawn "pactl set-sink-volume 0 +5%")
    , ("<XF86AudioMute>", spawn "pactl set-sink-mute 0 toggle")
    , ("<XF86MonBrightnessDown>", spawn "brightnessctl set 5%-")
    , ("<XF86MonBrightnessUp>", spawn "brightnessctl set 5%+")
    , ("<Print>", spawn "~/bin/scr")
    , ("M-<Print>", spawn "~/bin/scr -s")
    ]

-- gaps
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- prompts
myXPConfig :: XPConfig
myXPConfig = def { font = myFont
                 , bgColor = background
                 , fgColor = foreground
                 , bgHLight = color4
                 , fgHLight = background
                 , promptBorderWidth = 0
                 , position = Top
                 , height = 32
                 , searchPredicate = fuzzyMatch
                 , sorter = fuzzySort
                 }

-- theme for tabbed layouts
myTabTheme :: Theme
myTabTheme = def { fontName = myFont
                 , activeColor = color10
                 , inactiveColor = background
                 , activeTextColor = foreground
                 , inactiveTextColor = color8
                 , activeBorderColor = color10
                 , inactiveBorderColor = background
                 , decoHeight = 32
                 }

-- put all layouts together
myLayoutHook = avoidStruts $ mkToggle (MIRROR ?? FULL ?? EOT) ( tall ||| center ||| tabs )
  where
    tabs = R.renamed [R.Replace "tabs"] 
           $ BW.boringWindows 
           $ windowNavigation 
           $ tabbed shrinkText myTabTheme
    center = R.renamed [R.Replace "center"]
             $ windowNavigation
             $ BW.boringWindows
             $ minimize
             $ addTabs shrinkText myTabTheme
             $ subLayout [] (Simplest ||| Accordion)
             $ MG.magnifierczOff 1.2
             $ mySpacing 5
             $ ThreeColMid 1 (1/50) (1/2)
    tall = R.renamed [R.Replace "tall"]
           $ windowNavigation
           $ BW.boringWindows
           $ minimize
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (Simplest ||| Accordion)
           $ MG.magnifierczOff 1.2
           $ mySpacing 5
           $ ResizableTall 1 (1/50) (1/2) []

-- where to put my windows
myManageHook :: ManageHook
myManageHook = composeAll
    [ className =? "qutebrowser" --> doShift (myWorkspaces !! 1)
    , className =? "Chromium" --> doShift (myWorkspaces !! 8)
    , className =? "mpv" --> doShift (myWorkspaces !! 7)
    ]

-- do this on startup
myStartupHook :: X ()
myStartupHook = do
    spawnOnce "brightnessctl set 10%"
    spawnOnce "~/bin/night nyc &"
    spawnOnce "xautolock -time 10 -locker /home/patrick/bin/lock -detectsleep &"
    spawnOnce "picom &"
    spawnOnce "dunst &"
    spawnOnce "~/bin/input.sh"
    spawnOnce "~/.fehbg"
    spawnOnce "xsetroot -cursor_name left_ptr"
    spawnOnce "~/bin/init_keyboard"

-- transparency
myFadeHook :: FadeHook
myFadeHook = composeAll [ opaque
                        , isUnfocused --> transparency 0.10
                        , className =? "mpv" --> opaque
                        ]

-- workspaces with clickable actions
myWorkspaces :: [[ Char ]]
myWorkspaces = [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "]

-- statusbar and pretty printing
myPP :: PP
myPP =  def { -- ppOutput = hPutStrLn xmproc
              ppCurrent = xmobarColor background color4
            , ppVisible = xmobarColor background color10
            , ppHidden = xmobarColor background color5
            , ppHiddenNoWindows = xmobarColor "#e0def4" ""
            , ppUrgent = xmobarColor background color1
            , ppTitle = xmobarColor color5 "" . shorten 50
            , ppLayout = xmobarColor color4 "" . shorten 50
            , ppSep = "<fc=#9e9ab6> | </fc>"
            , ppWsSep = ""
            , ppOrder = id
            }
mySB :: StatusBarConfig
mySB = statusBarProp "xmobar" (clickablePP myPP)

-- this is where all the good stuff happens
main :: IO ()
main = do
    xmonad 
    $ withSB mySB 
    . docks 
    . ewmh 
    . ewmhFullscreen 
    $ myConfig
  where
    myConfig = def { modMask = myModMask
                   , terminal = myTerminal
                   , layoutHook = myLayoutHook
                   , workspaces = myWorkspaces
                   , manageHook = myManageHook <+> manageDocks -- don't ask me what <+> does
                   , handleEventHook = handleEventHook def <+> fadeWindowsEventHook
                   , logHook = fadeWindowsLogHook myFadeHook
                   , borderWidth = 0
                   , normalBorderColor = color0
                   , focusedBorderColor = color10
                   , startupHook = myStartupHook
                   } `additionalKeysP` myKeys
